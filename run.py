from aiohttp import web

from gw2api import app

web.run_app(app, host='0.0.0.0', port=8000)
