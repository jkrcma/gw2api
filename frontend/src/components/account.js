import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';

import useGW2Api from '../api/gw2api';

import './account.css';


const AccountInfoBox = ({apiKey}) => {
  const endpoint = 'account';
  const { data, isLoading, hasError } = useGW2Api(endpoint, apiKey, 0);
  let spinner = null;
  let guildList = null;

  const wrapper = (el) => <div className="account-infobox">{el}</div>;

  if (isLoading) {
    if (!Object.keys(data || {}).length) {
      return wrapper(<>Loading&hellip;</>);
    }

    spinner = <Spinner animation="border" variant="danger" size="sm" role="status" className="spinner-left" />;
  }

  if (hasError) {
    return wrapper("Failed to load account information :(");
  }

  if (data.guilds.length) {
    guildList = (
      <Col className="col-12 col-sm-12 col-lg-6 guilds">
        Guilds: {data.guilds.map((guild, i) => [i > 0 && ", ", <span key={i}><i>{guild.name}</i> <code>{guild.tag}</code></span>])}
      </Col>
    );
  }

  return wrapper(
    <>
      {spinner}
      <Alert variant="light" className="p-0 mb-0">
        <Row className="row-cols-2 row-cols-sm-2 row-cols-lg-12">
          <Col className="col-6 col-sm-6 col-lg-3">Name: <strong>{data.name}</strong></Col>
          <Col className="col-6 col-sm-6 col-lg-3">World: <strong>{data.world.name}</strong></Col>
          {guildList}
        </Row>
      </Alert>
    </>
  );
};

export default AccountInfoBox;
