import React from 'react';

import { Link, useLocation } from 'react-router-dom';

import Alert from 'react-bootstrap/Alert';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Fade from 'react-bootstrap/Fade'
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import InputGroup from 'react-bootstrap/InputGroup';

import { ArrowClockwise, EyeFill, EyeSlashFill } from 'react-bootstrap-icons';

import './common.css';


const useStateGW2ApiKey = () => {
  const LOCAL_STORAGE_KEY = 'GW2ApiKey';
  const [apiKey, setApiKey] = React.useState(
    localStorage.getItem(LOCAL_STORAGE_KEY) || ''
  );

  React.useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, apiKey);
    setApiKey(apiKey);
  }, [apiKey]);

  return [apiKey, setApiKey];
}


const FlashMessageContext = React.createContext({
  message: null,
  addMessage: () => {},
  removeMessage: () => {},
});


const useFlashMessage = () => {
  const {message, addMessage, removeMessage} = React.useContext(FlashMessageContext);
  return {message, addMessage, removeMessage};
};


function FlashMessageProvider({children}) {
  const [message, setMessage] = React.useState(null);
  const removeMessage = () => setMessage(null);
  const addMessage = (message, type) => setMessage({message, type});
  const contextValue = {
    message,
    addMessage: React.useCallback((message, type) => addMessage(message, type), []),
    removeMessage: React.useCallback(() => removeMessage(), []),
  };

  return <FlashMessageContext.Provider value={contextValue}>{children}</FlashMessageContext.Provider>;
}


const FlashMessageBox = () => {
  const {message, removeMessage} = useFlashMessage();
  let innerContent = <Alert className="invisible">.</Alert>;

  if (message) {
    innerContent = <Alert variant={message.type} onClick={removeMessage} onClose={removeMessage} dismissible>{message.message}</Alert>;
    setTimeout(removeMessage, 5000);
  }

  return (
    <Fade in={!!message}>
      <div>{innerContent}</div>
    </Fade>
  );
};


const Navigation = ({apiKey, onApiKeyChange, doRefresh}) => {
  const onChange = e => {
    onApiKeyChange(e.target.value);
    doRefresh();
  }
  const [showKey, setShowKey] = React.useState(false);

  const apiKeyToggle = showKey ? <EyeSlashFill /> : <EyeFill />;
  const apiKeyInputType = showKey ? 'text' : 'password';

  let activeLink = useLocation().pathname;

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand>GW2API</Navbar.Brand>
      <Navbar.Toggle aria-controls="top-navbar-nav" />
      <Navbar.Collapse id="top-navbar-nav">
        <Nav defaultActiveKey="/" activeKey={activeLink} className="mr-auto">
          <Nav.Item>
            <Nav.Link as={Link} to="/" href="/">Home</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link as={Link} to="/trading" href="/trading">Trading</Nav.Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
      <div className="box-apiKey d-inline-flex">
        <Navbar.Text className="mr-0 ml-auto">
          <Nav.Link as={Link} to={activeLink}><ArrowClockwise size={22} onClick={doRefresh} /></Nav.Link>
        </Navbar.Text>
        <Form inline className="flex-grow-1">
          <InputGroup className="w-100">
            <FormControl className="input-apiKey" type={apiKeyInputType} placeholder="API key&hellip;" value={apiKey} onChange={onChange} />
            <InputGroup.Append onClick={() => setShowKey(!showKey)}>
              <InputGroup.Text>{apiKeyToggle}</InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </div>
    </Navbar>
  );
}

export { useStateGW2ApiKey };
export { FlashMessageContext, FlashMessageProvider, FlashMessageBox, useFlashMessage };
export default Navigation;
