import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Collapse from 'react-bootstrap/Collapse';
import Image from 'react-bootstrap/Image';
import Spinner from 'react-bootstrap/Spinner';

import { ChevronDown, ChevronUp } from 'react-bootstrap-icons';

import { useFlashMessage } from './common';
import useGW2Api from '../api/gw2api';

import coinC from './assets/coin_c.png';
import coinS from './assets/coin_s.png';
import coinG from './assets/coin_g.png';
import './common.css';
import './trading.css';

const Amount = ({total}) => {
  let copper = total % 100
  let silver = (total % 10000 - copper) / 100
  let gold = (total - silver * 100 - copper) / 10000

  return (
    <span className="coin-amt">
      <span className="coin coin-g">{gold}</span><img src={coinG} alt="g" />&nbsp;
      <span className="coin coin-s">{silver}</span><img src={coinS} alt="s" />&nbsp;
      <span className="coin coin-c">{copper}</span><img src={coinC} alt="c" />
    </span>
  );
};

const InfoBox = ({title, data, textOverride = null}) => {
  return <CollapsibleInfoBox title={title} data={data} textOverride={textOverride} />
};

const CollapsibleInfoBox = ({title, data, textOverride = null, collapsedKey = null, collapsedRenderer = null}) => {
  const [open, setOpen] = React.useState(false);

  let subtitle = <i>{'\u00A0'}</i>;
  let text = textOverride ? <i className="small">{textOverride}</i> : <Amount total={data.total} />;
  let collapsedButton = null;
  let collapsedData = null;

  if (typeof(data.item_count) !== 'undefined') {
    subtitle = data.item_count + " items";
  }

  if (collapsedRenderer) {
    if (collapsedKey && typeof(data[collapsedKey]) !== 'undefined' && data[collapsedKey].length) {
      let Renderer = collapsedRenderer;
      let ButtonIcon = open ? ChevronUp : ChevronDown;
      collapsedButton = (
        <Button className="btn-success btn-xs ml-auto mr-0" onClick={() => setOpen(!open)} aria-expanded={open}>
          <ButtonIcon size={12} />
        </Button>
      );
      collapsedData = (
        <Collapse className="mt-4" in={open}>
          <div>
            <Renderer data={data[collapsedKey]} />
          </div>
        </Collapse>
      );
    }
  }

  return (
    <Card className="trading-infobox mb-3 mb-sm-4">
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle className="mb-3 text-muted">{subtitle}</Card.Subtitle>
        <Card.Text as="div">
          <span className="d-flex">
            {text}
            {collapsedButton}
          </span>
          {collapsedData}
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

const DeliveryItemList = ({data}) => {
  const renderer = (item, i) => {
    let counter = null;
    if (item.count > 1) {
      counter = <span class="item-count">{item.count}</span>;
    }
    return (
      <div className={"item rarity-" + item.rarity} key={i}>
        <Image src={item.icon} title={item.name} className="rounded" width={64} />
        {counter}
      </div>
    );
  };
  const itemsRendered = data.map(renderer);

  return (
    <div className="d-flex flex-wrap">
      {itemsRendered}
    </div>
  );
};

const TradingPostBilance = ({apiKey, refresh}) => {
  const endpoint = 'trading/bilance';
  const { data, isLoading, hasError } = useGW2Api(endpoint, apiKey, refresh);
  const { addMessage } = useFlashMessage();
  const prevDataRef = React.useRef([]);
  let spinner = null;
  let cachedData = {};

  React.useEffect(() => {
    if (hasError) {
      addMessage("Failed to load trading post bilance data :(", 'danger');
    } else {
      // maintain previous data
      prevDataRef.current = data;
    }
  }, [data, hasError, addMessage]);

  Object.assign(cachedData, prevDataRef.current || {}, data);

  if (isLoading) {
    if (!Object.keys(cachedData || {}).length) {
      return (
        <Row>
          <Col>
            Loading&hellip;
          </Col>
        </Row>
      );
    }

    spinner = <Spinner animation="border" variant="danger" size="sm" role="status" className="spinner-left" />;
  }

  if (hasError && !Object.keys(cachedData).length) {
    return <></>;
  }

  const totalWealth = Object.keys(cachedData)
      .map(key => cachedData[key])
      .reduce((a, b) => a + (typeof(b.total) !== 'undefined' ? b.total : 0), 0);

  return (
    <Container>
      {spinner}
      <Row>
        <Col>
          <div className="alert alert-secondary">Grand total account wealth: <Amount total={totalWealth} /></div>
        </Col>
      </Row>
      <Row className="row-cols-1 row-cols-sm-2 row-cols-lg-4">
        <Col>
          <InfoBox title="Buying" data={cachedData.buying} />
        </Col>
        <Col>
          <InfoBox title="Selling" data={cachedData.selling} />
        </Col>
        <Col>
          <CollapsibleInfoBox title="Delivery" data={cachedData.delivery} collapsedKey="items" collapsedRenderer={DeliveryItemList} />
        </Col>
        <Col>
          <InfoBox title="Wallet" data={cachedData.wallet} textOverride={cachedData.wallet.error} />
        </Col>
        <div className="w-100"></div>
      </Row>
    </Container>
  );
};

export default TradingPostBilance;
