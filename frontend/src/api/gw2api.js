import { useEffect, useState } from 'react';

const GW2API_VERSION = 'v1';

const useGW2Api = (endpoint, apiKey, refresh) => {
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const [fetchedData, setFetchedData] = useState();

  useEffect(() => {
    let unmounted = false;

    const handleFetchResponse = response => {
      if (unmounted) {
        return {};
      }

      setHasError(!response.ok);

      return response.ok && response.json ? response.json() : {};
    };

    const fetchData = async () => {
      let headers = {};

      if (apiKey) {
        headers["Authorization"] = "Bearer " + apiKey;
      }

      setIsLoading(true);

      return await fetch('/api/' + GW2API_VERSION + '/' + endpoint, { headers })
        .then(handleFetchResponse)
        .catch(handleFetchResponse);
    };

    if (!unmounted) {
      fetchData().then(data => {
        setFetchedData(data);
        setIsLoading(false);
      });
    }

    return () => {
      unmounted = true;
    };
  }, [endpoint, apiKey, refresh]);

  return { isLoading, hasError, data: fetchedData };
};

export default useGW2Api;
