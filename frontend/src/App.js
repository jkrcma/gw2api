import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


import AccountInfoBox from './components/account';
import Navigation, { useStateGW2ApiKey, FlashMessageProvider, FlashMessageBox } from './components/common';
import TradingPostBilance from './components/trading';

import './App.css';


const AppRouter = () => {
  const [apiKey, setApiKey] = useStateGW2ApiKey();
  const [refresh, setRefresh] = React.useState(0);
  const doRefresh = () => {
    setRefresh(refresh + 1);
  }

  return (
    <Router>
      <Row className="mb-2">
        <Col>
          <Navigation apiKey={apiKey} onApiKeyChange={apiKey => setApiKey(apiKey)} doRefresh={doRefresh} />
        </Col>
      </Row>
      <Row>
        <Col>
          <AccountInfoBox apiKey={apiKey} />
        </Col>
      </Row>
      <Row>
        <Col>
          <Container className="App-body">
            <Switch>
              <Route exact path="/">
                This project is in alpha version. Do not expect stuff to be working :)
              </Route>
              <Route path="/trading">
                <TradingPostBilance apiKey={apiKey} refresh={refresh} />
              </Route>
            </Switch>
          </Container>
        </Col>
      </Row>
    </Router>
  );
};


const Footer = () => (
  <Row className="mt-2">
    <Col>
      <hr/>
      <small>Copyright &copy; Tvoje Bába 2020</small>
    </Col>
  </Row>
)


const App = () => (
  <Container className="p-3">
    <FlashMessageProvider>
      <FlashMessageBox />
      <AppRouter />
    </FlashMessageProvider>
    <Footer />
  </Container>
);

export default App;
