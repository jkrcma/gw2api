FROM python:3.9-alpine

WORKDIR /app

RUN apk --update --no-cache --virtual add gcc musl-dev

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY run.py .
COPY gw2api gw2api

ENTRYPOINT ["python3", "run.py"]
