import asyncio
from hashlib import md5
from itertools import chain
from os import getenv
from math import ceil
from typing import Tuple

import aiohttp
import aioredis
import bson
from aiohttp import ClientSession

API_BASE_URL = 'https://api.guildwars2.com/v2/'


class ForbiddenException(Exception):
    pass

class RedisCache(object):
    class ClientResponseProxy(dict):
        """
        Serializable wrapper for `aiohttp.ClientResponse`. Only `headers` field is supported.
        """
        def __init__(self, resp: aiohttp.ClientResponse=None, *args, **kwargs):
            super(RedisCache.ClientResponseProxy, self).__init__(*args, **kwargs)
            self.__dict__ = self
            if resp:
                self.headers = dict(resp.headers)

        @staticmethod
        def from_dict(d):
            obj = RedisCache.ClientResponseProxy()
            obj.headers = d.get('headers', {})
            return obj

    def __init__(self, pool: aioredis.ConnectionsPool):
        self.pool = pool

    @staticmethod
    def _encode(obj):
        return bson.dumps({'r': obj})

    @staticmethod
    def _decode(s):
        return bson.loads(s).get('r') or {}

    async def save_request(self, cache_key, data, resp: aiohttp.ClientResponse, ttl):
        await self.pool.hmset_dict(
            cache_key,
            data=self._encode(data),
            response=self._encode(self.ClientResponseProxy(resp))
        )
        if ttl:
            await self.pool.expire(cache_key, ttl)

    async def load_request(self, cache_key):
        cache_data = await self.pool.hgetall(cache_key)

        if not cache_data:
            return

        return (
            self._decode(cache_data[b'data']),
            self.ClientResponseProxy.from_dict(self._decode(cache_data[b'response']))
        )

class GameApi(object):
    SOCK_TIMEOUT = 3
    READ_TIMEOUT = 5

    def __init__(self, api_key=None):
        self.api_key = api_key

    async def __aenter__(self):
        self.redis = await aioredis.create_redis_pool(getenv('REDIS_DSN', 'redis://redis'))
        headers = {'Authorization': self.api_key} if self.api_key else None
        self.session = ClientSession(headers=headers)

        return self

    async def __aexit__(self, *excinfo):
        self.redis.close()
        await asyncio.gather(self.redis.wait_closed(), self.session.close())

    @staticmethod
    def api_url(endpoint):
        return API_BASE_URL + endpoint

    @property
    def session_timeout(self):
        return aiohttp.ClientTimeout(sock_connect=self.SOCK_TIMEOUT, sock_read=self.READ_TIMEOUT)

    async def request(self, endpoint, page=None, page_size=None, ttl=120) -> Tuple[dict, aiohttp.ClientResponse]:
        # TODO: check for Last-Modified/If-Modified-Since
        #async with self.session.head(GameApi.api_url(endpoint)) as resp:
        #    pass
        redis_cache = RedisCache(self.redis)
        cache_key_parts = [self.api_key or '', endpoint] + [str(page), str(page_size)]
        cache_key = md5('|'.join(cache_key_parts).encode()).hexdigest()

        cache_data = await redis_cache.load_request(cache_key)
        if cache_data:
            return cache_data

        print(endpoint, page, page_size)
        params = {'page_size': page_size, 'page': page}
        async with self.session.get(
            GameApi.api_url(endpoint),
            params=dict(filter(lambda x: x[1] is not None, params.items())),
            timeout=self.session_timeout
        ) as resp:
            data = await resp.json()
            if resp.status in [400, 401, 403]:
                raise ForbiddenException()

        await redis_cache.save_request(cache_key, data, resp, ttl)
        return data, resp

    async def request_with_paging(self, endpoint, concurrency=5, min_page_size=50, max_page_size=200):
        # Initial request, needs to be sync so we get the information about the following pages, if any
        data, resp = await self.request(endpoint, 0, 1)
        results_total = int(resp.headers.get('X-Result-Total', 0))

        if not results_total:
            return data

        # Prepare slices accordingly what header did we get
        page_size = max(min_page_size, min(max_page_size, int(results_total / concurrency)))
        pages_total = ceil(results_total / page_size)

        # Fetch remaining pages asynchronously
        tasks = [self.request(endpoint, page, page_size) for page in range(0, int(pages_total))]
        results = await asyncio.gather(*tasks, return_exceptions=True)

        out = [data]
        failed = []
        for data2, resp in results:
            if type(data2) is not list:
                failed.append(data2)
            else:
                out.append(data2)

        if failed:
            # TODO: Error logging
            import pprint
            pprint.pprint(failed)

        return list(chain(*out))

class ApiModel(object):
    def __init__(self, game_api: GameApi):
        self.game_api = game_api

class AccountModel(ApiModel):
    TTL_LONG = 3600
    TTL_SHORT = 300

    async def get_account_info(self):
        account, _ = await self.game_api.request('account', ttl=AccountModel.TTL_LONG)

        tasks = []
        if account['world']:
            tasks.append(self.game_api.request('worlds/%s' % account['world'], ttl=AccountModel.TTL_LONG))
        if account['guilds']:
            tasks += [self.game_api.request('guild/' + gid, ttl=AccountModel.TTL_SHORT) for gid in account['guilds']]

        results = await asyncio.gather(*tasks, return_exceptions=True)

        world_data = {}
        guilds_data = []
        failed = []
        for i, (data, _) in enumerate(results):
            if type(data) is not dict:
                failed.append(data)
                continue

            if i == 0:
                world_data = data
            elif i >= 1:
                guilds_data.append(data)

        if failed:
            # TODO: Error logging
            import pprint
            pprint.pprint(failed)

        return {
            'name': account['name'],
            'world': world_data,
            'guilds': [{'name': guild['name'], 'tag': guild['tag']} for guild in guilds_data],
        }

class ItemModel(ApiModel):

    async def get_item(self, item_id):
        return self.get_items([item_id])

    async def get_items(self, item_ids: list):
        data, _ = await self.game_api.request('items?ids=' + ','.join([str(i) for i in item_ids]))

        return [{
            'id': item['id'],
            'name': item['name'],
            'icon': item['icon'],
            'rarity': item['rarity'].lower()}
        for item in data]

class TradingModel(ApiModel):
    # Refers to https://api.guildwars2.com/v2/currencies
    COIN_ID = 1

    def __init__(self, *args, **kwargs):
        super(TradingModel, self).__init__(*args, **kwargs)
        self.item_model = ItemModel(self.game_api)

    async def get_trading_bilance(self):
        def sum_goods(data):
            return {
                'item_count': len(data),
                'total': sum([item['price'] * item['quantity'] for item in data]),
            }

        def sum_delivery(data, items):
            return {
                'item_count': sum([item['count'] for item in data['items']]),
                'total': data['coins'],
                'items': items,
            }

        # given API key might not allow access to wallet
        wallet = {}
        try:
            wallet_data, _ = await self.game_api.request('account/wallet')
        except ForbiddenException:
            wallet = {'error': "Can't access account wallet data"}
        else:
            wallet_gold = [item['value'] for item in wallet_data if item['id'] == self.COIN_ID]
            if wallet_gold:
                wallet = {'total': wallet_gold[0]}

        delivery, _ = await self.game_api.request('commerce/delivery')
        items_idx = {item['id']: item for item in delivery['items']}
        if items_idx:
            for item_data in await self.item_model.get_items(list(items_idx.keys())):
                items_idx[item_data['id']].update(item_data)

        return {
            'wallet': wallet,
            'buying': sum_goods(await self.game_api.request_with_paging('commerce/transactions/current/buys')),
            'selling': sum_goods(await self.game_api.request_with_paging('commerce/transactions/current/sells')),
            'delivery': sum_delivery(delivery, list(items_idx.values())),
        }


