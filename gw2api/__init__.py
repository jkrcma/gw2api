from aiohttp import web

from gw2api.middlewares import unhandled_exception_response
from gw2api.routes import setup_routes

app = web.Application(middlewares=[unhandled_exception_response], debug=True)
setup_routes(app)
