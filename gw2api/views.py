import json
from functools import wraps

from aiohttp import web

from gw2api.models import AccountModel, ForbiddenException, GameApi, TradingModel


def error_response(exc: web.HTTPException, message):
    raise exc(text=json.dumps({'error': message}), content_type='application/json')


def requires_authorization(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        api_key = request.headers.get("Authorization")

        if not api_key or not api_key.replace('Bearer', '').strip():
            return error_response(web.HTTPUnauthorized, 'Missing GW 2 API bearer token')

        return func(request, api_key, *args, **kwargs)
    return wrapper


async def index(request):
    return web.Response(text='Hello, world!')


@requires_authorization
async def account(request, api_key):
    try:
        async with GameApi(api_key) as api:
            account = AccountModel(api)
            return web.json_response(data=await account.get_account_info())
    except ForbiddenException:
        raise error_response(web.HTTPForbidden, 'Invalid GW 2 API bearer token')


@requires_authorization
async def trading(request, api_key):
    try:
        async with GameApi(api_key) as api:
            trading = TradingModel(api)
            return web.json_response(data=await trading.get_trading_bilance())
    except ForbiddenException:
        raise error_response(web.HTTPForbidden, 'Invalid GW 2 API bearer token')
