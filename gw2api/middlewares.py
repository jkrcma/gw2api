from aiohttp import web

from gw2api.views import error_response


@web.middleware
async def unhandled_exception_response(request, handler):
    try:
        return await handler(request)
    except web.HTTPException:
        # pass through the well-defined exceptions
        raise
    except Exception as e:
        import traceback
        traceback.print_exc()
        return error_response(web.HTTPInternalServerError, 'Server was unable to handle your request')
