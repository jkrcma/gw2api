from gw2api.views import account, index, trading

def setup_routes(app):
    app.router.add_get('/', index)
    app.router.add_get('/v1/account', account)
    app.router.add_get('/v1/trading/bilance', trading)
